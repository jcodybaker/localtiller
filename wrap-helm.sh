#!/bin/bash
set -e

# Allow passing in a kubernetes config file.
KUBECONFIG="${KUBECONFIG:-/root/.kube/config}"

# TODO - Pull the namespace from the config file if one exists.

if test -z "${KUBERNETES_NAMESPACE}"; then 
  echo "KUBERNETES_NAMESPACE is required."
  exit 1
fi

if test ! -e "${KUBECONFIG}"; then
  if test -z "${KUBERNETES_CA_B64}"; then 
    echo "KUBECONFIG or KUBERNETES_CA_B64 are required."
    exit 1
  fi
  if test -z "${KUBERNETES_SERVER}"; then 
    echo "KUBECONFIG or KUBERNETES_SERVER are required."
    exit 1
  fi
  if test -z "${KUBERNETES_TOKEN_B64}"; then 
    echo "KUBECONFIG or KUBERNETES_TOKEN_B64 are required."
    exit 1
  fi
  echo "${KUBERNETES_CA_B64}" | base64 -d > /root/kubeca.crt
  kubectl config set-cluster kubernetes --server="${KUBERNETES_SERVER}" --certificate-authority=/root/kubeca.crt
  kubectl config set-credentials gitlab --token=$(echo ${KUBERNETES_TOKEN_B64} | base64 -d)
  kubectl config set-context kubernetes --user=gitlab --cluster=kubernetes --namespace="${KUBERNETES_NAMESPACE}"
  kubectl config use-context kubernetes
fi

export HELM_HOST="localhost:44134"
TILLER_NAMESPACE=${KUBERNETES_NAMESPACE} /usr/local/bin/tiller -listen ${HELM_HOST} &
TILLER_PID=$!

# Tiller's in the background, we need to wait for it to be ready. Wait 100s
echo "Waiting for tiller"
for each in $(seq 100); do
  if nc -z localhost 44134; then TILLER_READY=true; break; fi
  sleep 0.1
done
if test "${TILLER_READY}" = true; then
  echo "Tiller launched."
else 
  echo "Tiller failed to launch."
  exit 1
fi

set +e
/bin/bash -c "$@"
RC=$?
set -e
kill "${TILLER_PID}"
exit $RC