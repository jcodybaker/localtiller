# localtiller

[Helm recommends](https://docs.helm.sh/using_helm/#securing-your-helm-installation) either running tiller with TLS authentication, OR running tiller locally.

This docker image configures the environment, starts a local tiller, waits for it to be ready, executes the provided command, and then cleans up.

## Usage:
```
docker run -it --rm \
  -e KUBERNETES_NAMESPACE=${KUBERNETES_NAMESPACE} \
  -e KUBERNETES_CA_B64=${KUBERNETES_CA_B64} \
  -e KUBERNETES_TOKEN_B64=${KUBERNETES_TOKEN_B64} \
  -e KUBERNETES_SERVER=${KUBERNETES_SERVER} \
  registry.gitlab.com/jcodybaker/localtiller:latest \
  helm install nginx
```

It is expected that KUBERNETES_CA_B64 contains a base64 encoded version of the Kubernetes CA certificate pem file.
KUBERNETES_TOKEN_B64 should contain a base64 encoded kubernetes authentication token.