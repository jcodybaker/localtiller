FROM ubuntu:bionic

RUN apt-get update && apt-get install -y \
    curl \
    netcat \
 && rm -rf /var/lib/apt/lists/*

RUN curl -Lo /usr/local/bin/kubectl https://storage.googleapis.com/kubernetes-release/release/v1.12.2/bin/linux/amd64/kubectl && \
    curl -Ls https://storage.googleapis.com/kubernetes-helm/helm-v2.11.0-linux-amd64.tar.gz | tar -xzvf - && \
    cp linux-amd64/helm linux-amd64/tiller /usr/local/bin/ && \
    rm -rf linux-amd64 && \
    chmod +x /usr/local/bin/kubectl /usr/local/bin/helm /usr/local/bin/tiller 

COPY ./wrap-helm.sh /usr/local/bin/

# This seems to break gitlab CI for now. 
#ENTRYPOINT ["/usr/local/bin/wrap-helm.sh"]
